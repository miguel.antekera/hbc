## HBC
Shopify theme using Slate Framework.

## Local requirements
* Slate framwework [npm install -g @shopify/slate]
* Node version 6.x

## Installation
* [git clone] 
* [slate watch] 

## Available Slate commands
    theme | t [options] [name]  Generates a new theme directory containing Slate's theme boilerplate.
    migrate [options]           Converts an existing theme to work with Slate.
    build | b                   Compiles source files (<theme>/src/) into the format required for distribution to a Shopify store (<theme>/dist/).
    deploy | d [options]        Runs a full deploy of your theme's code to a Shopify store specified in config.yml. Existing files will be overwritten.
    start | s [options]         Runs a clean build & deploy of the theme's source files to a Shopify store specified in config.yml, then starts file watch and live-reload tasks, allowing for immediate updates during development.
    test                        Runs translation tests for a theme's locale files (<theme>/src/locales/).
    watch | w [options]         Watches files for code changes and immediately deploys updates to your store as they occur. By default, this command also runs a live-reload proxy that refreshes your store URL in-browser when changes are successfully deployed.
    zip | z                     Rebuilds the theme's source files and compresses the output. The compres


## Documentation

Sage documentation is available at [https://shopify.github.io/slate/](https://shopify.github.io/slate/).

## Author

Miguel Antequera
mike@acid.cl
January 2017